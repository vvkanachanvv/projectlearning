package jp.dcl.manhole.menu;

import java.util.HashMap;

import jp.dcl.manhole.android.R;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;

/**
 * メニュー画面管理クラス
 * @author Kanako
 *
 */
public class MenuController extends LinearLayout {

/**
 * メニュー画面切替時callback用ハンドラー
 * @see #callback(ContentState)
 */
private static final Handler handler = new Handler();
/**
 * メニュー画面の状態変数
 * @see ContentState
 */
public static ContentState nowContentState = ContentState.ASSIGNMENT;
/**
 * メニュー画面の状態変数nowContentStateと、実際のレイアウトを対応させるマップ
 * メニューに新たなレイアウトを追加するときはここに入れる
 */
public static HashMap<ContentState, View> content_map = new HashMap<ContentState, View>();
/**
 * メニュー画面の実際のレイアウトを格納するコンテント
 */
public static LinearLayout content;

/**
 * メニュー画面状態遷移用enum定数
 * @author Kanako
 *
 */
public enum ContentState {
	ASSIGNMENT,
	INFORMATION,
	ANALYSIS,
	PRESENTATION
}
		
	/**
	 * コンストラクタ。
	 * 各メニュー内のレイアウトはここでcontent_mapにputすること
	 * @param context
	 */
	public MenuController(Context context) {
		super(context);

		View child = inflate(context, R.layout.menu_base_layout, null);
		this.addView(child);
		content = (LinearLayout)(child.findViewById(R.id.layout_menu_content));

        content_map.put(ContentState.ASSIGNMENT, inflate(context, R.layout.menu_assignment_layout, null));
        content_map.put(ContentState.INFORMATION, inflate(context, R.layout.menu_information_layout, null));
        content_map.put(ContentState.ANALYSIS, inflate(context, R.layout.menu_analysis_layout, null));
        content_map.put(ContentState.PRESENTATION, inflate(context, R.layout.menu_presentation_layout, null));

        content.addView(content_map.get(nowContentState));
	}
	
	/**
	 * メニューの切り替えボタンがクリックされたときの処理
	 * @param state
	 */
	public static void callback(final ContentState state) {
		handler.post(new Runnable() {

			@Override
			public void run() {
				if(state == nowContentState) return;
				View con = content_map.get(state);
				content.removeAllViews();
				content.addView(con);
				
				nowContentState = state;
			}
		});
	}
}