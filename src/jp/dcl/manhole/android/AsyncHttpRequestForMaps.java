package jp.dcl.manhole.android;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageButton;

public class AsyncHttpRequestForMaps extends AsyncTask<String, Integer, Bitmap> {

	private ImageButton button;
	
	public AsyncHttpRequestForMaps(ImageButton button) {
		this.button = button;
	}

    // ここが非同期で処理される部分みたいたぶん。
    @Override
    protected Bitmap doInBackground(String... url) {
        Bitmap bmp = null;
        HttpClient httpclient = new DefaultHttpClient();   
        HttpGet request = new HttpGet(url[0]); 

        InputStream in = null;
        try {
            in = httpclient.execute(request).getEntity().getContent();
            bmp = BitmapFactory.decodeStream(in);
            in.close();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bmp;
    }

	@Override
	protected void onProgressUpdate(Integer... progress) {
		Log.d("Async", ""+progress[0]);
	}

    // このメソッドは非同期処理の終わった後に呼び出されます
    @Override
    protected void onPostExecute(Bitmap result) {
		button.setImageBitmap(result);
    }
}
