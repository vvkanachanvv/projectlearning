package jp.dcl.manhole.android;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

public class MyGlobal {
	
	// Constant Global
	public static final BitmapDescriptor question_icon = BitmapDescriptorFactory.fromResource(R.drawable.dialog_question);
	public static final BitmapDescriptor star_icon = BitmapDescriptorFactory.fromResource(R.drawable.golden_star);

//	public static final MarkerOptions[] POS = {
//		new MarkerOptions().position(new LatLng(35.706003,139.706697)).title("早稲田大学").icon(question_icon),
//		new MarkerOptions().position(new LatLng(35.718878, 139.776507)).title("東京国立博物館").icon(question_icon),
//		new MarkerOptions().position(new LatLng(35.716367, 139.776532)).title("国立科学博物館").icon(question_icon),
//		new MarkerOptions().position(new LatLng(35.686862, 139.766923)).title("逓信総合博物館").icon(question_icon),
//		new MarkerOptions().position(new LatLng(35.685404, 139.771664)).title("貨幣博物館").icon(question_icon),
//		new MarkerOptions().position(new LatLng(35.69139, 139.752966)).title("科学技術館").icon(question_icon),
//		new MarkerOptions().position(new LatLng(35.696477, 139.796065)).title("江戸東京博物館").icon(question_icon),
//		new MarkerOptions().position(new LatLng(35.681185, 139.800592)).title("深川江戸資料館").icon(question_icon),
//		new MarkerOptions().position(new LatLng(35.715449, 139.512512)).title("江戸東京たてもの園").icon(question_icon),
//		new MarkerOptions().position(new LatLng(35.68996, 139.72588)).title("新宿歴史博物館").icon(question_icon)
//	};
	
	public static final int ACTIVITY_CODE_DETAIL = 0;
	public static final int ACTIVITY_CODE_SEARCH_ON_GOOGLE = 1;
	
	public static Bitmap static_map = null;

    public static boolean isPortrait(Activity activity) {
        return (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
    }
	
//	public static void gotoMapActivity(Activity parent) {
//		Intent intent = new Intent(parent, jp.hischro.MyMapActivity.class);
//		parent.startActivity(intent);
//	}
//
//	public static void gotoCameraActivity(Activity parent, String title) {
//		Intent intent = new Intent(parent, jp.hischro.MyCameraActivity.class);
//		intent.setAction(Intent.ACTION_SEND);
//		intent.putExtra("title", title);
//		parent.startActivityForResult(intent, ACTIVITY_CODE_DETAIL);
//	}
//	
//	public static void gotoDetailActivity(Activity parent, String title) {
//		Intent intent = new Intent(parent, jp.hischro.MyDetailActivity.class);
//		intent.setAction(Intent.ACTION_SEND);
//		intent.putExtra("title", title);
//		parent.startActivity(intent);
//	}
//	
//	public static void gotoChronicleActivity(Activity parent) {
//		Intent intent = new Intent(parent, jp.hischro.MyChronicleActivity.class);
//		parent.startActivity(intent);
//	}
//	
//	public static void gotoSearchOnGoogle(Activity parent, String title) {
//		Intent intent = new Intent(parent, jp.hischro.MyWebView.class);
//		intent.setAction(Intent.ACTION_SEND);
//		intent.putExtra("title", title);
//		parent.startActivityForResult(intent, ACTIVITY_CODE_DETAIL);
//	}
//	
//	public static void gotoImageActivity(Activity parent, String url, String title) {
//		Intent intent = new Intent(parent, jp.hischro.ImageActivity.class);
//		intent.setAction(Intent.ACTION_SEND);
//		intent.putExtra("url", url);
//		intent.putExtra("title", title);
//		parent.startActivityForResult(intent, ACTIVITY_CODE_SEARCH_ON_GOOGLE);
//	}
//	
	public static void goBackToOriginalActivity(Activity parent, String title) {
		Intent rtnIntent = new Intent();
		rtnIntent.setAction(Intent.ACTION_SEND);
		rtnIntent.putExtra("title", title);
		parent.setResult(Activity.RESULT_OK, rtnIntent);
		parent.finish();
	}
	
}
