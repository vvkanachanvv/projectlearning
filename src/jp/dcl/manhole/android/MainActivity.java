package jp.dcl.manhole.android;

import jp.dcl.manhole.contents_view.BaseContentView;
import jp.dcl.manhole.contents_view.CameraContentView;
import jp.dcl.manhole.contents_view.DayActivityLayout;
import jp.dcl.manhole.contents_view.MapContentView;
import jp.dcl.manhole.contents_view.MemoContentView;
import jp.dcl.manhole.contents_view.StickerContentView;
import jp.dcl.manhole.contents_view.TodoContentView;
import jp.dcl.manhole.menu.MenuController;
import jp.dcl.manhole.menu.MenuController.ContentState;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.OnHierarchyChangeListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.MapsInitializer;

/**
 * メインのアクティビティクラス。
 * 各レイアウトからのタッチイベントの親はこのクラスなので、ここで設定の必要あり。
 * @author Kanako
 * @see BaseContentView
 */
public class MainActivity extends Activity {
/**
 * デバッグ用タグ
 */
private static final String TAG = "Manhole";
/**
 * メインレイアウト用
 */
private LinearLayout _main = null;
/**
 * 活動日レイアウト
 */
public LinearLayout _dayActivity = null;
/**
 * 付箋メモ用レイアウト
 */
public FrameLayout _frameLayout = null;


	/**
	 * コンストラクタ。<br>
	 * 
	 * <i>変更の必要なし</i><br>
	 * レイアウトをxmlから設定する。
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// プロジェクト名を取得
		Intent intent = getIntent();
        if(intent != null){
            this.setTitle(intent.getStringExtra("project_title"));
            Log.d("Manhole", intent.getStringExtra("project_title"));
        }
		
		LinearLayout menu = (LinearLayout)this.findViewById(R.id.layout_menu);
		menu.addView(new MenuController(this));

		_main = (LinearLayout)this.findViewById(R.id.layout_main);

		_main.setOnHierarchyChangeListener(new OnHierarchyChangeListener() {

			@Override
			public void onChildViewAdded(View parent, View child) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onChildViewRemoved(View parent, View child) {
				if(child.equals(_dayActivity)) {
					Log.d("MainActivity", "dayactivity is removed");
					_dayActivity = null;
				}
			}
		});
		
		// Check Maps initializer
		try {
	        MapsInitializer.initialize(getApplicationContext());
	    } catch (GooglePlayServicesNotAvailableException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	}
	
	
	/**
	 * メニューレイアウトの遷移ボタンクリック時のイベント<br>
	 * 
	 * <i>変更の必要なし</i><br>
	 * メニューレイアウトに押されたボタンに対応するレイアウト(View)を貼りなおす。<br>
	 * 例)「課題」ボタンのクリック＝＞メモ、付せん…などのボタンが表示される
	 *
	 * @param view イベントコーラーのView(自動生成)
	 */
	public void onClickMenu(View view) {
		switch(view.getId()){
			case R.id.button_assignment:
				MenuController.callback(ContentState.ASSIGNMENT); 
				break;
			case R.id.button_information:
				MenuController.callback(ContentState.INFORMATION); 
				break;
			case R.id.button_analysis:
				MenuController.callback(ContentState.ANALYSIS); 
				break;
			case R.id.button_presentation:
				MenuController.callback(ContentState.PRESENTATION);
				break;
		}
	}
	
	/**
	 * 各メニューレイアウト内のボタンのクリック時のイベント<br>
	 * ここでクリックされたボタンに応じてメインレイアウト内に機能が追加される<br>
	 * _main.addView(new [機能実装クラス名](this, _main, [xmlファイル名]));を実装のこと。<br>
	 * (case R.id.button_memo参照)<br>
	 * 
	 * @param view イベントコーラーのView(自動生成)
	 */
	public void onClickMenuButton(View view) {
		if(_main == null) return;
		if(_dayActivity == null) {
			_dayActivity = new DayActivityLayout(this);
			_main.addView(_dayActivity);
		}
		switch(view.getId()) {
		// 課題設定
		case R.id.button_memo:
			_dayActivity.addView(new MemoContentView(this, R.layout.content_memo_layout));
			break;
		case R.id.button_sticker:
			_dayActivity.addView(new StickerContentView(this, R.layout.content_sticker_layout_2));
			break;
		case R.id.button_todo:
			_dayActivity.addView(new TodoContentView(this, R.layout.content_todo_layout));
			break;
		case R.id.button_flowchart:
			break;
			
		// 情報の収集
		case R.id.button_sketch:
			break;
		case R.id.button_internet:
			break;
		case R.id.button_camera:
			_dayActivity.addView(new CameraContentView(this, R.layout.content_camera_layout));
			break;
		case R.id.button_mic:
			break;
		case R.id.button_interview:
			break;
		case R.id.button_map:
			_dayActivity.addView(new MapContentView(this, R.layout.content_map_layout));
			break;

		// 整理・分析
		case R.id.button_classify:
			break;
		case R.id.button_graph:
			break;

		// 発表・まとめ
		case R.id.button_presentation_tool:
			break;
		}
	}
}
