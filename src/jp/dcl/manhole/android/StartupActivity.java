package jp.dcl.manhole.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 起動時に最初に呼ばれるアクティビティ
 * @author Kanako
 * @see android.app.Activity
 */
public class StartupActivity extends Activity {
	/**
	 * 自クラス。ダイアログ用。
	 */
	Activity startupActivity;

	/**
	 * コンストラクタ
	 * <i>起動時に勝手に呼ばれるので気にする必要なし。</i>
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_startup);
		startupActivity = this;
	}

	/**
	 * <i>自動生成メソッド。気にする必要なし</i>
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manhole_main, menu);
		return true;
	}
	
	/**
	 * 「新しいプロジェクト」ボタンクリック時のイベント
	 * @param view イベントコーラーのView(自動生成)
	 * @see MainActivity
	 */
	public void doStartNewProject(View view) {
		final EditText project_title = new EditText(this);
		project_title.setHint("新規プロジェクト名");
		new AlertDialog.Builder(this)
		.setTitle("新規プロジェクト")
		.setMessage("プロジェクト名を入力してください！")
		.setView(project_title)
		.setPositiveButton("OK", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(project_title.getText().toString().equals("")) {
					Toast.makeText(startupActivity, "プロジェクト名を指定してください。", Toast.LENGTH_SHORT).show();
					return;
				}
		    	Intent intent = new Intent(startupActivity, jp.dcl.manhole.android.MainActivity.class);
		    	intent.putExtra("project_title", project_title.getText().toString());
		    	startupActivity.startActivity(intent);
			}
		})
		.show();

	}
	
	/**
	 * 「プロジェクトを開く」ボタンクリック時のイベント
	 * @param view イベントコーラーのView(自動生成)
	 */
	public void doOpenProject(View view) {
		// open project
	}
}
