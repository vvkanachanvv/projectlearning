package jp.dcl.manhole.database;

import java.util.ArrayList;
import java.util.Map;

import jp.dcl.manhole.contents.MemoContent;

public interface DataBaseAccessMethodInterface {
	public ArrayList<PlaceInfo> getAllPlaces();
	public PlaceInfo getPlaceInfo(int id);
	public PlaceInfo getPlaceInfo(String title);
	public ArrayList<String> getTitlesOrderByTime();
	public void setPicPath(String title, String path);
	public void setVisited(String title, boolean visited);
	public void setVisited(int id, boolean visited);
	public void setMemo(int id, String memo);
	public void setMemo(String title, String memo);
	public ArrayList<Map<String, String>> getItemsForChronicle();
	
	public MemoContent getMemoContent(int id);
	

}
