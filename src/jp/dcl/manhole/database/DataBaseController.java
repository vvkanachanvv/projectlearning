package jp.dcl.manhole.database;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import jp.dcl.manhole.contents.MemoContent;

import android.content.Context;
import android.database.SQLException;
import android.util.Log;

public class DataBaseController	implements DataBaseAccessMethodInterface {
	private static final String TAG = "DataBaseController";
	private DataBaseHelper mDbHelper;  
	
	public DataBaseController(Context context) {
		mDbHelper = new DataBaseHelper(context);
	}

	public void setDatabase(Context context) {  
    	Log.i(TAG, "onCreate");
	    try {  
	        mDbHelper.createEmptyDataBase();  
	    } catch (IOException ioe) {  
	        throw new Error("Unable to create database");  
	    } catch(SQLException sqle){  
	        throw sqle;
	    }
	}
	
	public void openDatabase(Context context) {
		mDbHelper.openDataBase();
	}
	
	public void closeDatabase() {
		if(mDbHelper != null)
		mDbHelper.close();
	}
	
	@Override
	public ArrayList<PlaceInfo> getAllPlaces() {
		if(mDbHelper == null) new ArrayList<PlaceInfo>();
		return mDbHelper.getAllPlaces();
	}
	
	@Override
	public PlaceInfo getPlaceInfo(int id) {
		return mDbHelper.getPlaceInfo(id);
	}
	
	@Override
	public PlaceInfo getPlaceInfo(String title) {
		return mDbHelper.getPlaceInfo(title);
		
	}
	
	@Override
	public ArrayList<String> getTitlesOrderByTime() {
		return mDbHelper.getTitlesOrderByTime();
	}
	
	@Override
	public void setPicPath(String title, String path) {
		Log.d(TAG, "setPicPath");
		mDbHelper.setPicPath(title, path);
	}
	
	@Override
	public void setVisited(String title, boolean visited) {
		mDbHelper.setVisited(title, visited);
	}
	
	@Override
	public void setVisited(int id, boolean visited) {
		mDbHelper.setVisited(id, visited);
	}

	@Override
	public void setMemo(int id, String memo) {
		mDbHelper.setMemo(id, memo);
		
	}

	@Override
	public void setMemo(String title, String memo) {
		mDbHelper.setMemo(title, memo);
	}

	@Override
	public ArrayList<Map<String, String>> getItemsForChronicle() {
		return mDbHelper.getItemsForChronicle();
	}

	@Override
	public MemoContent getMemoContent(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
