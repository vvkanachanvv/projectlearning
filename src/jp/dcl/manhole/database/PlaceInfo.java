package jp.dcl.manhole.database;

import java.io.Serializable;

public class PlaceInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	public int id;
	public double lat;
	public double lng;
	public String title;
	public boolean visited;
	
	public String memo;
	public String pic_path;
	public int time;
	public boolean can_take_pic;

	public PlaceInfo(int id, double lat, double lng, String title, boolean visited, String memo, String pic_path, int time, boolean can_take_pic) {
		this.id = id;
		this.lat = lat;
		this.lng = lng;
		this.title = title;
		this.visited = visited;
		this.memo = memo;
		this.pic_path = pic_path;
		this.time = time;
		this.can_take_pic = can_take_pic;
	}

	public PlaceInfo(int id, double lat, double lng, String title, boolean visited) {
		this(id, lat, lng, title, visited, null, null, -1, true);
	}


}