package jp.dcl.manhole.database;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jp.dcl.manhole.contents.MemoContent;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
  
public class DataBaseHelper 
	extends SQLiteOpenHelper 
	implements DataBaseAccessMethodInterface{  
	
	private static final String TAG = "jp.hischro:DataBaseHelper";
  
    //The Android のデフォルトでのデータベースパス  
    private static String DB_NAME = "hischro";  
    private static String DB_NAME_ASSET = "hischro.db";
    private static int DB_VERSION = 1;
	private static final String DB_TABLE = "places";
	private static final String COL_ID = "_id";
	private static final String COL_LAT = "lat";
	private static final String COL_LNG = "lng";
	private static final String COL_TITLE = "title";
	private static final String COL_VISITED = "visited";
	private static final String COL_MEMO = "memo";
	private static final String COL_PIC_PATH = "pic_path";
	private static final String COL_TIME = "time";
	private static final String COL_CAN_TAKE_PIC = "can_take_pic";
	private static final String COL_ALL = "*";
   
    private SQLiteDatabase mDataBase;   
   
    private final Context mContext;  
      
      
    public DataBaseHelper(Context context) {  
        super(context, DB_NAME, null, DB_VERSION);    
        this.mContext = context;  
    }
    
    public String getmDataBaseName() {
    	return DB_NAME;
    }
    
  
    /** 
     * asset に格納したデータベースをコピーするための空のデータベースを作成する 
     *  
     **/  
    public void createEmptyDataBase() throws IOException{  
        boolean mDataBaseExist = checkDataBaseExists();  
  
        if(mDataBaseExist){  
            // すでにデータベースは作成されている  
        	Log.i(TAG, "mDataBaseExist");
        }else{  
            // このメソッドを呼ぶことで、空のデータベースが  
            // アプリのデフォルトシステムパスに作られる  
        	Log.i(TAG, "creating empty mDataBase");
            this.getReadableDatabase();  
   
            try {  
                // asset に格納したデータベースをコピーする  
                copyDataBaseFromAsset();   
            } catch (IOException e) {  
                throw new Error("Error copying database");  
            }  
        }  
    }   

    /** 
     * 再コピーを防止するために、すでにデータベースがあるかどうか判定する 
     *  
     * @return 存在している場合 {@code true} 
     */  
    private boolean checkDataBaseExists() {  
        SQLiteDatabase checkmDataBase = null;  
   
        try{  
	    	String myPath = mContext.getDatabasePath(DB_NAME).toString();
	        checkmDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        }catch(SQLiteException e){  
            // データベースはまだ存在していない  
        }
   
        if(checkmDataBase != null){  
            checkmDataBase.close();  
        }  
        return checkmDataBase != null ? true : false;  
    }      
   
    /** 
     * asset に格納したデーだベースをデフォルトの 
     * データベースパスに作成したからのデータベースにコピーする 
     * */  
    private void copyDataBaseFromAsset() throws IOException{  
   
        // asset 内のデータベースファイルにアクセス  
        InputStream mInput = mContext.getAssets().open(DB_NAME_ASSET);  
   
        // デフォルトのデータベースパスに作成した空のmDataBase  
        File outFile = mContext.getDatabasePath(DB_NAME);
   
        OutputStream mOutput = new FileOutputStream(outFile);  
  
        // コピー  
        byte[] buffer = new byte[1024];
        int size;
        while ((size = mInput.read(buffer)) > 0){
            mOutput.write(buffer, 0, size);
        }
   
        //Close the streams  
        mOutput.flush();  
        mOutput.close();  
        mInput.close();  
    }  
   
    public SQLiteDatabase openDataBase() throws SQLException{  
        //Open the database  
    	String myPath = mContext.getDatabasePath(DB_NAME).toString();
        mDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        return mDataBase;  
    }  
      
    @Override  
    public void onCreate(SQLiteDatabase arg0) {  
    }
    
    @Override  
    public void onUpgrade(SQLiteDatabase mDataBase, int oldVersion, int newVersion) {  
    }  
  
    @Override  
    public synchronized void close() {  
        if(mDataBase != null)  
            mDataBase.close();  
      
        super.close();  
    }  
    
	//===================================
	// データアクセス用関数
	//===================================
    
    // ===== GoogleMaps Marker用データ取得 ===
    public ArrayList<PlaceInfo> getAllPlaces() {
    	ArrayList<PlaceInfo> allPlaces = new ArrayList<PlaceInfo>();
    	if(mDataBase != null) {
    		String[] searchQuery = new String[]{COL_ID, COL_LAT, COL_LNG, COL_TITLE, COL_VISITED};
    		
	    	Cursor cursor = null;
	    	try {
	    		cursor = mDataBase.query(DB_TABLE, searchQuery, null, null, null, null, null);
	    		
	    		while(cursor.moveToNext()) {
	    			allPlaces.add(new PlaceInfo(
	    					cursor.getInt(0),
	    					cursor.getDouble(1),
	    					cursor.getDouble(2),
	    					cursor.getString(3),
	    					cursor.getInt(4) == 1 ? true:false
	    					));
	    		}
	    	} finally {
	    		if( cursor != null )
	    			cursor.close();
	    	}
    	}

    	return allPlaces;
    }
    
    // ===== 訪問済みチェック変更 w/ id ====
    public void setVisited(int id, boolean visited) {
    	if(mDataBase == null) return;

    	ContentValues val = new ContentValues();
        val.put( COL_VISITED, visited?1:0 );

        mDataBase.update( DB_TABLE, 
                val,
                COL_ID + " = ?",
                new String[]{"" + id} );
    }

    // ===== 訪問済みチェック変更 w/ title ====
    public void setVisited(String title, boolean visited) {
    	if(mDataBase == null) return;

    	ContentValues val = new ContentValues();
        val.put( COL_VISITED, visited?1:0 );

        mDataBase.update( DB_TABLE, 
                val,
                COL_TITLE + " = ?",
                new String[]{title} );
    }
    
    // ==== placesエントリー取得 w/ id ====
    public PlaceInfo getPlaceInfo(int id) {
    	PlaceInfo place = null;
    		if(mDataBase !=null) {
    			String[] searchQuery = new String[]{COL_ALL};
    		
		    	Cursor cursor = null;
		    	try {
		    		cursor = mDataBase.query(DB_TABLE, searchQuery, COL_ID + "=?", new String[]{""+id}, null, null, null);
		    		
		    		while(cursor.moveToNext()) {
		    			place = new PlaceInfo(
		    					cursor.getInt(0),
		    					cursor.getDouble(1),
		    					cursor.getDouble(2),
		    					cursor.getString(3),
		    					cursor.getInt(4) == 1 ? true:false,
								cursor.getString(5),
								cursor.getString(6),
								cursor.getInt(7),
								cursor.getInt(8) == 1 ? true:false
		    					);
		    		}
		    	} finally {
		    		if( cursor != null )
		    			cursor.close();
		    	}
    		}
    	return place;
    }

    // ==== placesエントリー取得 w/ title ====
    public PlaceInfo getPlaceInfo(String title) {
    	PlaceInfo place = null;
    		if(mDataBase !=null) {
    			String[] searchQuery = new String[]{COL_ALL};
    		
		    	Cursor cursor = null;
		    	try {
		    		cursor = mDataBase.query(DB_TABLE, searchQuery, COL_TITLE + "=?", new String[]{title}, null, null, null);
		    		
		    		while(cursor.moveToNext()) {
		    			place = new PlaceInfo(
		    					cursor.getInt(0),
		    					cursor.getDouble(1),
		    					cursor.getDouble(2),
		    					cursor.getString(3),
		    					cursor.getInt(4) == 1 ? true:false,
								cursor.getString(5),
								cursor.getString(6),
								cursor.getInt(7),
								cursor.getInt(8) == 1 ? true:false
		    					);
		    		}
		    	} finally {
		    		if( cursor != null )
		    			cursor.close();
		    	}
    		}
    	return place;
    }
    
    // ==== 写真ファイル設定 ====
    public void setPicPath(String title, String path) {
    	if(mDataBase == null) {
    		Log.e(TAG, "mDataBase is null");
    		return;
    	}

		Log.d(TAG, "setPicPath");

    	ContentValues val = new ContentValues();
        val.put( COL_PIC_PATH, path );

        mDataBase.update( DB_TABLE, 
                val,
                COL_TITLE + "=?",
                new String[]{title} );
    	
    }
    
    // ==== 年代順にタイトルを取得。リストビュー用 ====
    public ArrayList<String> getTitlesOrderByTime() {
    	ArrayList<String> titles = new ArrayList<String>();
    	if(mDataBase != null) {
    		String[] searchQuery = new String[]{COL_TITLE};
    		
	    	Cursor cursor = null;
	    	try {
	    		cursor = mDataBase.query(DB_TABLE, searchQuery, null, null, null, null, COL_TIME);
	    		
	    		while(cursor.moveToNext()) {
	    			titles.add(cursor.getString(0));
	    		}
	    	} finally {
	    		if( cursor != null )
	    			cursor.close();
	    	}
    	}

    	return titles;
    }
    
    @Override
    public ArrayList<Map<String, String>> getItemsForChronicle() {
    	ArrayList<Map<String, String>> rtnArray = new ArrayList<Map<String, String>>();
    	if(mDataBase != null) {
    		String[] searchQuery = new String[]{COL_TIME,COL_TITLE,COL_VISITED};
    		
	    	Cursor cursor = null;
	    	try {
	    		cursor = mDataBase.query(DB_TABLE, searchQuery, null, null, null, null, COL_TIME);
	    		Map<String, String> map;
	    		while(cursor.moveToNext()) {
	    			map = new HashMap<String, String>();
	    			map.put("time", cursor.getString(0));
	    			map.put("title", cursor.getString(1));
	    			map.put("visited", cursor.getString(2));
	    			rtnArray.add(map);
	    		}
	    	} finally {
	    		if( cursor != null )
	    			cursor.close();
	    	}
    	}
    	
    	return rtnArray;
    }

	@Override
	public void setMemo(int id, String memo) {
    	if(mDataBase == null) {
    		Log.e(TAG, "mDataBase is null");
    		return;
    	}

		Log.d(TAG, "setPicPath");

    	ContentValues val = new ContentValues();
        val.put( COL_MEMO, memo );

        mDataBase.update( DB_TABLE, 
                val,
                COL_ID + "=?",
                new String[]{"" + id} );
    	
	}

	@Override
	public void setMemo(String title, String memo) {
    	if(mDataBase == null) {
    		Log.e(TAG, "mDataBase is null");
    		return;
    	}

		Log.d(TAG, "setPicPath");

    	ContentValues val = new ContentValues();
        val.put( COL_MEMO, memo);

        mDataBase.update( DB_TABLE, 
                val,
                COL_TITLE + "=?",
                new String[]{title} );
    	
		
	}

	@Override
	public MemoContent getMemoContent(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}