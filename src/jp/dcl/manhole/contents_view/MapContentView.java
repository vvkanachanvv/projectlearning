package jp.dcl.manhole.contents_view;

import jp.dcl.manhole.android.AsyncHttpRequestForMaps;
import jp.dcl.manhole.android.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

/**
 * コンテンツクラス　メモの実装
 * @author Kanako
 * @see BaseContentView
 *
 */
public class MapContentView extends BaseContentView {
//	private DataBaseController dbc;
	Activity parent;
	/**
	 * 
	 * @param context
	 * @param content_layout_id
	 */
	public MapContentView(final Context context, int content_layout_id) {
		super(context, content_layout_id);
		parent = (Activity)context;
		
		setupInfo();
		((ImageButton)child.findViewById(R.id.content_map_imagebutton_map))
			.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context, jp.dcl.manhole.contents_view.MyMapActivity.class);
					intent.setAction(Intent.ACTION_SEND);
						((Activity)context).startActivityForResult(intent, 0);
					}
		});
	}

	private void setupInfo() {
//		dbc = new DataBaseController(this);
//		dbc.openDatabase(this);
//		PlaceInfo place = dbc.getPlaceInfo(title);
//		dbc.closeDatabase();
//		dbc = null;
//
//        preparePicture(place);
        
//        if(place.memo != null) {
//        	EditText memo = (EditText)this.findViewById(R.id.detail_memo);
//        	memo.setText(place.memo);
//        }

		ImageButton button = ((ImageButton)child.findViewById(R.id.content_map_imagebutton_map));
		setGoogleMapThumbnail(button, 35.706003, 139.706697);
	}
/**
	 * Borrowed from http://stackoverflow.com/questions/5324004/how-to-display-static-google-map-on-android-imageview
	 * @param lati
	 * @param longi
	 * @return
	 */
	private void setGoogleMapThumbnail(ImageButton button, double lati, double longi){
        String URL = "http://maps.google.com/maps/api/staticmap?center=" +lati + "," + longi + 
        		"&zoom=15&size=600x300&scale=2&sensor=false&key=AIzaSyCykIzAMHHdPqpbv1kWI0z-vRsDpdmcRAM";
        Log.d("MyGlobal", "URL = "+URL);
		AsyncHttpRequestForMaps task = new AsyncHttpRequestForMaps(button);
		task.execute(URL);
    }	
}
