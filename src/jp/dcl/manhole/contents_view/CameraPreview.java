package jp.dcl.manhole.contents_view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import jp.dcl.manhole.android.MyGlobal;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, PictureCallback{

    private Camera mCam;
    public boolean mIsTake = false;
    private MyCameraActivity myCameraActivity;
    private boolean mPreviewing = false;
	private final String TAG = "CameraPreview";
	private SurfaceHolder mSurfaceHolder;

    /**
     * コンストラクタ
     */
	@SuppressWarnings("deprecation")
	public CameraPreview(Context context, Camera cam) {
        super(context);
        myCameraActivity = (MyCameraActivity)context;

        mCam = cam;

        // サーフェスホルダーの取得とコールバック通知先の設定
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    /**
     * SurfaceView 生成
     */
    public void surfaceCreated(SurfaceHolder holder) {
        mSurfaceHolder = holder;
        // カメラインスタンスに、画像表示先を設定
    	if(mPreviewing && holder.isCreating()){
            setPreviewDisplay(holder);
        } else {
            startCamera();
        }
    }

    /**
     * SurfaceView 破棄
     */
    public void surfaceDestroyed(SurfaceHolder holder) {
    	stopCamera();
        mSurfaceHolder = null;
    }

    /**
     * SurfaceHolder が変化したときのイベント
     */
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    	setCameraSettings();
    }
    
    
    private void setCameraSettings(){
    	if(mCam!=null) {
    		Log.i(TAG, "setCameraSettings");
    		Camera.Parameters params = mCam.getParameters();
    		Camera.Size pictureSize;
    		Camera.Size previewSize;

    		//端末がサポートするサイズを取得する
    		List<Size> supportedPictureSizes 
    					= params.getSupportedPictureSizes();
    		List<Size> supportedPreviewSizes 
    					= params.getSupportedPreviewSizes();
    				
    		if ( supportedPictureSizes != null &&
    			supportedPreviewSizes != null &&
    			supportedPictureSizes.size() > 0 &&
    			supportedPreviewSizes.size() > 0) {
    			
    			//2.xの場合

    			//撮影サイズを設定する 一番小さいサイズで画像を保存する
//    			pictureSize = supportedPictureSizes.get(supportedPictureSizes.size()-1);
//    			previewSize = supportedPictureSizes.get(supportedPictureSizes.size()-1);
//    			Log.i(TAG, "pictureSize1 = "+pictureSize);
    	    	    	
    			pictureSize = supportedPictureSizes.get(supportedPictureSizes.size()-5);
//    			Log.i(TAG, "pictureSize2 = "+pictureSize);
    			previewSize = supportedPictureSizes.get(supportedPictureSizes.size()-5);

    			params.setPictureSize(pictureSize.width, pictureSize.height);		
    			params.setPreviewSize(previewSize.width, previewSize.height);
    	      
	    		// Set orientation
	    	    boolean portrait = MyGlobal.isPortrait(myCameraActivity);
	    	    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
	    	        // 2.1 and before
	    	        if (portrait) {
	    	            params.set("orientation", "portrait");
	    	        } else {
	    	            params.set("orientation", "landscape");
	    	        }
	    	    } else {
	    	        // 2.2 and later
	    	        if (portrait) {
	    	            mCam.setDisplayOrientation(90);
	    	        } else {
	    	            mCam.setDisplayOrientation(0);
	    	        }
	    	    }
	    	    
	    	    setPreviewSize(portrait, previewSize);
    		
    		} else {
    			Log.i(TAG, "picSize Somehow not working??");
    		}
    		this.mCam.setParameters(params);
    		try {
    			this.mCam.setPreviewDisplay(mSurfaceHolder);
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    		this.mCam.startPreview();
    	} else {
    		Log.i(TAG, "mCam is Null.");
    	}
    }
    
    public void setPreviewSize(boolean portrait, Camera.Size previewSize) {
		//一番小さいプレビューサイズを利用
		LayoutParams paramLayout;

		//ビューサイズをプレビューサイズに合わせる
		paramLayout = getLayoutParams();
		if(portrait) {
			paramLayout.width = previewSize.height;
			paramLayout.height = previewSize.width;
		} else {
			paramLayout.width = previewSize.width;
			paramLayout.height = previewSize.height;
		}
		setLayoutParams(paramLayout);
    }
    
    @Override
	public void onPictureTaken(byte[] data, Camera c) {
    	if (data == null) {
            return;
        }

        Toast.makeText(myCameraActivity, "写真を保存中...", Toast.LENGTH_SHORT).show();

//        String imgPath = savePicture(data);
//        
//        // takePicture するとプレビューが停止するので、再度プレビュースタート
////        mCam.startPreview();
//
//    	// 撮った写真のパスをDBにセットする
//        TextView tv = (TextView)myCameraActivity.findViewById(R.id.camera_title);
//        String title = tv.getText().toString();
//    	DataBaseController dbc = new DataBaseController(myCameraActivity);
//    	dbc.openDatabase(myCameraActivity);
//    	dbc.setPicPath(title, imgPath);
//    	dbc.setVisited(title, true);
//    	dbc.closeDatabase();
//		Log.d(TAG, "setPicPath");
//
//        Toast.makeText(myCameraActivity, "写真が撮れました！", Toast.LENGTH_SHORT).show();
//
//        mIsTake = false;
        
		/**
		 *  CameraPreview内のonPictureTakenは時間がかかるため、
		 *  Activityの遷移はコールバックとして用意しておく。
		 */ 
        myCameraActivity.callback();
    }
    
    private String savePicture(byte[] data) {
    	Matrix m = new Matrix(); //Bitmapの回転用Matrix
        m.setRotate(90);
        Bitmap original = BitmapFactory.decodeByteArray(data, 0, data.length);
        Bitmap rotated = Bitmap.createBitmap(
            original, 0, 0, original.getWidth(), original.getHeight(), m, true);
        
        String saveDir = Environment.getExternalStorageDirectory().getPath() + "/hischro";

        // SD カードフォルダを取得
        File file = new File(saveDir);

        // フォルダ作成
        if (!file.exists()) {
            if (!file.mkdir()) {
                Log.e("Debug", "Make Dir Error");
            }
        }
        
     // 画像保存パス
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String imgPath = saveDir + "/" + sf.format(cal.getTime()) + ".jpg";
        // ファイル保存
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imgPath, true);
            if(MyGlobal.isPortrait(myCameraActivity))
            	rotated.compress(CompressFormat.JPEG, 100, fos);
            else
            	original.compress(CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();

            // アンドロイドのデータベースへ登録
            // (登録しないとギャラリーなどにすぐに反映されないため)
            registAndroidDB(imgPath);

        } catch (Exception e) {
            Log.e("Debug", e.getMessage());
        }
        
        fos = null;
        
        return imgPath;
    }
    
    public void takePicture() {
		if(!mIsTake) {
			// 撮影中の2度押し禁止用フラグ
            mIsTake = true;
			mCam.takePicture(null,null,this);
		}
    }

	@Override
	public boolean onTouchEvent(MotionEvent me) {
		if(me.getAction()==MotionEvent.ACTION_DOWN) {
			if(!mIsTake) {
				// 撮影中の2度押し禁止用フラグ
	            mIsTake = true;
				mCam.takePicture(null,null,this);
			}
		}
		return true;
	}
	
	 /**
     * アンドロイドのデータベースへ画像のパスを登録
     * @param path 登録するパス
     */
    private void registAndroidDB(String path) {
        // アンドロイドのデータベースへ登録
        // (登録しないとギャラリーなどにすぐに反映されないため)
        ContentValues values = new ContentValues();
        ContentResolver contentResolver = myCameraActivity.getContentResolver();
        values.put(Images.Media.MIME_TYPE, "image/jpeg");
        values.put("_data", path);
        myCameraActivity.setImageURL(contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values));
    }
    
    void getCameraDevice(){
        if(mCam == null){
            mCam = Camera.open();
        }
    }
    
    void setPreviewDisplay(SurfaceHolder holder){
        Log.i(TAG,"setPreviewDisplay");
                //本当はちゃんとエラー処理が必要
        try {
            mCam.setPreviewDisplay(holder);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public void startCamera(){
        Log.i(TAG,"startCamera");
		if(mPreviewing){stopCamera();}
        getCameraDevice();
        if(!mPreviewing){
            Log.i(TAG,"preview_started");
            setCameraSettings();
            mPreviewing = true;
        }
    }
    
    public void stopCamera(){
        Log.i(TAG,"stopCamera");
                //onPauseやonDestroyで呼ばれる
                //でもここではSurfaceholderをnullしない
        if(mCam != null){
            Log.i(TAG,"releaseCam");
            mCam.stopPreview();
            mCam.release();
            mCam = null;
            mPreviewing = false;
        }
    }
}
