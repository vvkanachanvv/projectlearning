package jp.dcl.manhole.contents_view;

import jp.dcl.manhole.android.R;
import jp.dcl.manhole.contents.BaseContent;
import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * コンテンツクラス　付箋メモの実装
 * @author Takuya
 * @see BaseContent
 *
 */
public class Sticker extends BaseContentView {
	
	private boolean longClickFlg = true;   //長押しチェック用フラグ
	
	/**
	 * コンストラクタ
	 * 
	 * xml内にボタン（イベント）がある際には内部で必ず
	 * 	setTagForButtons((Button)child.findViewById([xml内ボタンid1, ボタンid2, ...]));
	 * を呼ぶこと！！！
	 * イベントの窓口はMainActivityに、実質の記述はここに書く
	 * 
	 * @param context イベントを管理する親アクティビティクラス
	 * @param parentLayout 親レイアウト。自クラスの削除に使用。
	 * @param content_layout_id この機能のレイアウト用xmlの指定
	 * @param longClickFlg
	 * @see BaseContent
	 * @see BaseContent#setTagForButtons(Button...)
	 */
	public Sticker(Context context, int content_layout_id) {
		super(context, content_layout_id, false);
		
//		addSticker(context, content_layout_id);
		
		// ボタンの設定をする
//		setTagForButtonsWithLayer((Button)child.findViewById(R.id.sticker_destroy));
		
		/*final OnLongClickListener longClick = new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				longClickFlg = true;
				return false;
			}		
		};*/
		
		final OnTouchListener moving = new OnTouchListener() {
			 
            private float downX;  //画面タッチ位置の座標：X軸
            private float downY;  //画面タッチ位置の座標：Y軸
 
            private int downLeftMargin;
            private int downTopMargin;
 
            @Override
            public boolean onTouch(View v, MotionEvent event) {
 
            	// ViewGroup.MarginLayoutParamsでキャストすることで
                // FrameLayoutの子要素であっても同様に扱える。
                final ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams)v.getLayoutParams();
 
                switch(event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    downX = event.getRawX();
                    downY = event.getRawY();
 
                    downLeftMargin = param.leftMargin;
                    downTopMargin = param.topMargin;
                    
                    Log.d("sticker", "ACTION_DOWN");
 
                    return true;
                case MotionEvent.ACTION_MOVE:
                	if(longClickFlg){
                		param.leftMargin = downLeftMargin + (int)(event.getRawX() - downX);
                		param.topMargin = downTopMargin + (int)(event.getRawY() - downY);

                		v.layout(
                				param.leftMargin,
                				param.topMargin,
                				param.leftMargin + v.getWidth(),
                				param.topMargin + v.getHeight());
                	}
                	//スクロール無効
                	v.getParent().getParent().getParent().getParent().requestDisallowInterceptTouchEvent(true);
                	Log.d("sticker", "ACTION_MOVE");
                	return true;
                default:
                	//longClickFlg = false;
                	//スクロール無効解除
                	v.getParent().getParent().getParent().getParent().requestDisallowInterceptTouchEvent(false);
                	Log.d("sticker", "その他");
                	break;
                }
                return false;
            }
        };
        
        //findViewById(R.id.sticker_base).setOnLongClickListener(longClick);
        findViewById(R.id.sticker_base).setOnTouchListener(moving);
	}
	
	/**
	 * 付箋メモ追加用関数<br>
	 * @param context イベントを管理する親アクティビティクラス
	 * @param parentLayout 親レイアウト。自クラスの削除に使用。
	 * @param content_layout_id この機能のレイアウト用xmlの指定
	 * @author Takuya
	 */
	public void addSticker(Context context, int content_layout_id) {
		
		child = inflate(context, content_layout_id, null);
		LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        this.setLayoutParams(param);
        this.setId(R.id.sticker_base);
        child.setId(R.id.sticker_base-1);
		this.addView(child);
	}
}
