package jp.dcl.manhole.contents_view;

import java.io.IOException;

import jp.dcl.manhole.android.MyGlobal;
import jp.dcl.manhole.android.R;
import jp.dcl.manhole.database.DataBaseController;
import jp.dcl.manhole.database.PlaceInfo;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MyDetailActivity extends Activity {
	final static String TAG="MyDetailActivity";
	private DataBaseController dbc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.detail_layout);

        Intent intent = this.getIntent();
		setupInfo(intent);
	}
	
	private void setupInfo(Intent intent) {
//		String title = null;
//        // get intent　場所のタイトル
//        if(intent.getAction().equals(Intent.ACTION_SEND)) {
//        	TextView tv_title = (TextView)this.findViewById(R.id.detail_title);
//        	title = intent.getStringExtra("title");
//        	tv_title.setText(title);
//        }
//
//		dbc = new DataBaseController(this);
//		dbc.openDatabase(this);
//		PlaceInfo place = dbc.getPlaceInfo(title);
//		dbc.closeDatabase();
//		dbc = null;
//
//        preparePicture(place);
//        
//        if(place.memo != null) {
//        	EditText memo = (EditText)this.findViewById(R.id.detail_memo);
//        	memo.setText(place.memo);
//        }
	}
	
	private void preparePicture(final PlaceInfo place) {
//		ImageView imgView = (ImageView)this.findViewById(R.id.detail_picture);
//		
//		// ==== パスから写真を取得 ====
//        if(place.pic_path != null) {
//        	int degree = 0;
//        	ExifInterface exifInterface;
//			try {
//				exifInterface = new ExifInterface(place.pic_path);
//			} catch (IOException e) {
//				e.printStackTrace();
//				return;
//			}
//        	int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_NORMAL);
//        	switch (orientation) {
//        	    case ExifInterface.ORIENTATION_ROTATE_90:
//        	        degree = 90;
//	        		break;
//        	    case ExifInterface.ORIENTATION_ROTATE_180:
//	        		degree = 180;
//	        		break;
//        	    case ExifInterface.ORIENTATION_ROTATE_270:
//	        		degree = 270;
//	        		break;
//        	    default:
//	        		break;
//        	}
//        	Bitmap original = BitmapFactory.decodeFile(place.pic_path);
//	        Matrix m = new Matrix(); //Bitmapの回転用Matrix
//	        m.setRotate(degree);
//	        Bitmap rotated = Bitmap.createBitmap(
//	                original, 0, 0, original.getWidth(), original.getHeight(), m, true);
//	        imgView.setImageBitmap(rotated);
//        } else
//        	imgView.setImageResource(R.drawable.hachune);
//        
//        // ==== 写真を撮れるか否かでリスナーの内容を付加 ====
//        imgView.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				if(place.can_take_pic) {
//					MyGlobal.gotoCameraActivity(MyDetailActivity.this, place.title);
//				} else {
//					MyGlobal.gotoSearchOnGoogle(MyDetailActivity.this, place.title);
//				}
//			}
//        });
        
	}
	
	@Override
	public void onPause() {
		if(dbc != null) {
			dbc.closeDatabase();
			dbc = null;
		}
		super.onPause();
	}
	
	public void searchDetail(View view) {
		
	}
	
	public void editDetail(View view) {
//		TextView tv = (TextView)this.findViewById(R.id.detail_title);
//		EditText ex = (EditText)this.findViewById(R.id.detail_memo);
//		DataBaseController dbc = new DataBaseController(this);
//		dbc.openDatabase(this);
//		dbc.setMemo(tv.getText().toString(), ex.getText().toString());
//		dbc.closeDatabase();
//		dbc = null;
//        Toast.makeText(MyDetailActivity.this, "メモを編集しました！", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		
		if(requestCode == MyGlobal.ACTIVITY_CODE_DETAIL) {
			if(resultCode == Activity.RESULT_OK) {
				setupInfo(intent);
			}
		}
	}
}
