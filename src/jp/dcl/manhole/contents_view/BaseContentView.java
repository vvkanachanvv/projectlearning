package jp.dcl.manhole.contents_view;

import jp.dcl.manhole.android.R;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * コンテンツ用抽象クラス<br>
 * コンテンツ用クラスを作成する際はこのクラスを継承すること。
 * @author Kanako
 *
 */
public abstract class BaseContentView extends LinearLayout {
	protected static int TagNumber = 0;
	protected View child;
	
	public BaseContentView(Context context) {
		super(context);
	}

	public BaseContentView(Context context, int content_layout_id, boolean yes_match_parent) {
		super(context);

		child = inflate(context, content_layout_id, null);
		if(yes_match_parent) {
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	        param.setMargins(5, 5, 5, 5);
	        child.setLayoutParams(param);
		}

		this.addView(child);

		Button button_destroy = ((Button)child.findViewById(R.id.button_destroy));
		if(button_destroy != null) {
			button_destroy.setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						destroyContent();		
				}});
		}
	}
	
	/**
	 * コンストラクタ<br>
	 * @param context イベントを管理する親アクティビティクラス
	 * @param parentLayout 親レイアウト。自クラスの削除に使用。
	 * @param content_layout_id この機能のレイアウト用xmlの指定
	 */
	public BaseContentView(Context context, int content_layout_id) {
		this(context, content_layout_id, true);
	}

	/**
	 * 自クラス削除用関数<br>
	 * 補足的な機能はこのクラスをオーバーライドして実装
	 */
	public void destroyContent() {
		((ViewGroup)this.getParent()).removeView(this);
	}
	
	/**
	 * レイアウト内のボタンと親ビューに同じタグを付ける<br>
	 * ボタンにアクションを登録するときには必ず設定する必要あり<br>
	 * @param buttons
	 * @see MainActivity#
	 */
	protected int setTagForButtons(Button... buttons) {
		// ボタンにタグを付ける
		for(int i=0; i<buttons.length; i++) {
			buttons[i].setTag(TagNumber);
		}
		this.setTag(TagNumber);
		return TagNumber++;
	}
	
	/**
	 * レイアウト内のボタンと親ビューに同じタグを付ける<br>
	 * ボタンにアクションを登録するときには必ず設定する必要あり<br>
	 * @param buttons
	 * @see MainActivity#
	 */
	protected int setTagForButtonsWithLayer(int parent_tag, Button... buttons) {
		StringBuffer tag_sb = new StringBuffer("");
		tag_sb.append(parent_tag);
		tag_sb.append("_");
		tag_sb.append(TagNumber);
		// ボタンにタグを付ける
		for(int i=0; i<buttons.length; i++) {
			buttons[i].setTag(tag_sb);
		}
		this.setTag(tag_sb);
		return TagNumber++;
	}
}