package jp.dcl.manhole.contents_view;

import jp.dcl.manhole.android.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;

/**
 * コンテンツクラス　メモの実装
 * @author Kanako
 * @see BaseContentView
 *
 */
public class CameraContentView extends BaseContentView {
//	private DataBaseController dbc;
	
	/**
	 * 
	 * @param context
	 * @param content_layout_id
	 */
	public CameraContentView(final Context context, int content_layout_id) {
		super(context, content_layout_id);
		
		((ImageButton)child.findViewById(R.id.content_camera_imagebutton_camera))
			.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context, jp.dcl.manhole.contents_view.MyCameraActivity.class);
					intent.setAction(Intent.ACTION_SEND);
						((Activity)context).startActivityForResult(intent, 0);
					}
		});
	}

	private void setupInfo() {
//		dbc = new DataBaseController(this);
//		dbc.openDatabase(this);
//		PlaceInfo place = dbc.getPlaceInfo(title);
//		dbc.closeDatabase();
//		dbc = null;
//
//        preparePicture(place);
        
//        if(place.memo != null) {
//        	EditText memo = (EditText)this.findViewById(R.id.detail_memo);
//        	memo.setText(place.memo);
//        }
	}
}
