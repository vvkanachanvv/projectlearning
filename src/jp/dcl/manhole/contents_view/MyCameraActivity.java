package jp.dcl.manhole.contents_view;

import java.net.URL;

import jp.dcl.manhole.android.MyGlobal;
import jp.dcl.manhole.android.R;
import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

public class MyCameraActivity extends Activity {
	final static String TAG="MyCameraActivity";
	// カメラインスタンス
    private Camera mCam = null;

    // カメラプレビュークラス
    private CameraPreview mCamPreview = null;
    
    private Uri imageURL;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_layout);
        
        // get intent　場所のタイトル
        Intent intent = this.getIntent();
//        if(intent.getAction().equals(Intent.ACTION_SEND)) {
//        	TextView tv_title = (TextView)this.findViewById(R.id.camera_title);
//        	title = intent.getStringExtra("title");
//        	tv_title.setText(title);
//        }

        // FrameLayout に CameraPreview クラスを設定
        FrameLayout preview = (FrameLayout)findViewById(R.id.cameraPreview);
        mCamPreview = new CameraPreview(this, mCam);
        preview.addView(mCamPreview);
        
    }
    
    @Override
    public void onPause(){
        Log.i(TAG,"onPause");
        super.onPause();
        mCamPreview.stopCamera();
    }
    
    @Override
    public void onResume(){
        Log.i(TAG,"onResume");
        mCamPreview.startCamera();
        super.onResume();
    }
        
    @Override
    public void onDestroy(){
        Log.i(TAG,"onDestroy");
        mCamPreview.stopCamera();
        super.onDestroy();
    }
    
    // ==== layoutからのメソッドコール ====
    public void takePicture(View view) {
    	mCamPreview.takePicture();
    }

	public void callback() {
		/** CameraPreview内のonPictureTakenは時間がかかるため、
		 *  Activityの遷移はコールバックとして用意しておく。
		 * 
		 */
//    	MyGlobal.gotoDetailActivity(this, title);
		MyGlobal.goBackToOriginalActivity(MyCameraActivity.this, "");
//		MyGlobal.goBackToOriginalActivity(MyCameraActivity.this, imageURL.toString());
		
	}
	
	public void setImageURL(Uri url) {
		this.imageURL = url;
	}
    
}
