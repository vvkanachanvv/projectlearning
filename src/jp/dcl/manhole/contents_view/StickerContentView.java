package jp.dcl.manhole.contents_view;

import jp.dcl.manhole.android.R;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

/**
 * コンテンツクラス　メモの実装
 * @author Kanako
 * @see BaseContentView
 *
 */
public class StickerContentView extends BaseContentView {
	Context parent;
	BaseContentView bcview;
	/**
	 * コンストラクタ
	 * 
	 * xml内にボタン（イベント）がある際には内部で必ず
	 * 	setTagForButtons((Button)child.findViewById([xml内ボタンid1, ボタンid2, ...]));
	 * を呼ぶこと！！！
	 * イベントの窓口はMainActivityに、実質の記述はここに書く
	 * 
	 * @param context イベントを管理する親アクティビティクラス
	 * @param parentLayout 親レイアウト。自クラスの削除に使用。
	 * @param content_layout_id この機能のレイアウト用xmlの指定
	 * @see BaseContentView
	 * @see BaseContentView#setTagForButtons(Button...)
	 */
	public StickerContentView(Context context, int content_layout_id) {
		super(context, content_layout_id);
		parent = context;
		bcview = this;
		
		final OnClickListener addSticker = new OnClickListener() {
			@Override
			public void onClick(View v) {
				((FrameLayout)(child.findViewById(R.id.sticker_frame)))
				.addView(new Sticker(parent, R.layout.content_sticker_layout));
		}};
			
		((Button)child.findViewById(R.id.button_sticker_add)).setOnClickListener(addSticker);
	}
}
