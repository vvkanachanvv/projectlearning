package jp.dcl.manhole.contents_view;

import android.content.Context;

/**
 * コンテンツクラス　メモの実装
 * @author Kanako
 * @see BaseContentView
 *
 */
public class MemoContentView extends BaseContentView {
	
	/**
	 * コンストラクタ
	 * 
	 * ボタンに付けるイベントはリスナーで定義する。
	 * 
	 * @param context イベントを管理する親アクティビティクラス
	 * @param content_layout_id この機能のレイアウト用xmlの指定
	 * @see BaseContentView
	 */
	public MemoContentView(Context context, int content_layout_id) {
		super(context, content_layout_id);
	}
}
