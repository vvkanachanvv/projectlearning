package jp.dcl.manhole.contents_view;

import java.util.ArrayList;

import jp.dcl.manhole.android.MyGlobal;
import jp.dcl.manhole.android.R;
import jp.dcl.manhole.database.PlaceInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MyMapActivity extends FragmentActivity implements OnInfoWindowClickListener{
	final static String TAG="MyMapActivity";
	private GoogleMap map;
//	private DataBaseController dbc;
	private static final int ZOOM_INIT = 13; // 16

	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.map_layout);
		setUpMapIfNeeded();
	}

	//  Find and initialize the map view.
	private void setUpMapIfNeeded() {
	// Do a null check to confirm that we have not already instantiated the map.
		if (map == null) {
		// Try to obtain the map from the SupportMapFragment.
		   map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		   // Check if we were successful in obtaining the map.
		   if (map != null) {
		      setUpMap();
		   }
		}
	}
	
	private void setUpMap() {
		// TODO バックキーで戻ってきたときに読み込み直す
		// set up map.
		LatLng POS_WASEDA = new LatLng(35.706003,139.706697);
		
		// 位置情報取得のためDBから情報を抜き取っておく
//		dbc = new DataBaseController(this);
//		dbc.openDatabase(this);
		ArrayList<PlaceInfo> places = new ArrayList<PlaceInfo>();
//		places = dbc.getAllPlaces();
//		dbc.closeDatabase();
//		dbc = null;
		
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(POS_WASEDA, ZOOM_INIT));
		map.setOnInfoWindowClickListener(this);
		LatLng latlng;
		
//		for(MarkerOptions m: MyGlobal.POS) {
//			map.addMarker(m);
//		}

		for(PlaceInfo p: places) {
			latlng = new LatLng(p.lat, p.lng);
			map.addMarker(new MarkerOptions()
			.position(latlng)
			.title(p.title)
			.icon(p.visited?MyGlobal.star_icon:MyGlobal.question_icon));
		}
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
//		Log.d(TAG, "onInfoWindowClick");
//		MyGlobal.gotoDetailActivity(this, marker.getTitle());
	}
	
	@Override
	public void onPause(){
//		if(dbc != null) {
//			dbc.closeDatabase();
//			dbc = null;
//		}
		super.onPause();
	}
	

 }
