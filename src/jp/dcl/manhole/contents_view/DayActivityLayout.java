package jp.dcl.manhole.contents_view;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import jp.dcl.manhole.android.R;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DayActivityLayout extends LinearLayout {
	public DayActivityLayout(Context context) {
		super(context);
		
		this.setBackgroundResource(R.color.skyblue);
        this.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        this.setOrientation(VERTICAL);
        this.setPadding(30, 30, 30, 30);
        
		
		// 日付を表示
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy'年'MM'月'dd'日'", Locale.JAPAN);
		TextView date_text = new TextView(context);
		date_text.setTextSize(22);
		date_text.setText(sdf.format(date));
		
		this.addView(date_text);
		
		this.setOnHierarchyChangeListener(new OnHierarchyChangeListener() {
			@Override
			public void onChildViewAdded(View parent, View child) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onChildViewRemoved(View parent, View child) {
				Log.d("DayActivityLayout", "Child is Removed.");
				LinearLayout thislayout = (LinearLayout)parent;
				if((thislayout).getChildCount() == 1) {
					Log.d("DayActivityLayout", "No Child");
					((LinearLayout)parent.getParent()).removeView(parent);
				}
			}
		});
	}
}
