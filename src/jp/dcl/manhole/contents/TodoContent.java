package jp.dcl.manhole.contents;

import java.io.Serializable;
import java.util.ArrayList;

public class TodoContent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id;
	public String title;
	public ArrayList<TodoElement> elements;
	
	public TodoContent(int id, String title, TodoElement[] elements) {
		this.id = id;
		this.title = title;
		for(int i=0; i<elements.length; i++) {
			this.elements.add(new TodoElement(
					elements[i].element_id, 
					elements[i].element_text,
					elements[i].element_complete));
		}
		
	}
	
	private class TodoElement {
		protected int element_id;
		protected String element_text;
		protected boolean element_complete;
		
		protected TodoElement(int element_id, String element_text, boolean element_complete) {
			this.element_id = element_id;
			this.element_text = element_text;
			this.element_complete = element_complete;
		}
	}
	
}