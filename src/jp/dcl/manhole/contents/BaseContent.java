package jp.dcl.manhole.contents;

import java.io.Serializable;


public class BaseContent implements Serializable {
	/**
	 * auto generated serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	public int id;
	public String title;
	public String text;
	
	public BaseContent(int id, String title, String text) {
		this.id = id;
		this.title = title;
		this.text = text;
	}
}

