package jp.dcl.manhole.contents;

public class MapContent extends BaseContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public MapContent(int id, String title, String text) {
		super(id, title, text);
	}
	
	private class Legend {
		protected int legend_type;
		protected String legend_text;
		protected String legend_icon_path;
		
	}

}
