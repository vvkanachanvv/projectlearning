package jp.dcl.manhole.contents;

public class SketchContent extends BaseContent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String sketch_url;

	public SketchContent(int id, String title, String text) {
		super(id, title, text);
	}

	public SketchContent(int id, String title, String text, String sketch_url) {
		this(id, title, text);
		this.sketch_url = sketch_url;
	}
}
