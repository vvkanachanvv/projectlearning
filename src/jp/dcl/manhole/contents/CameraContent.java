package jp.dcl.manhole.contents;

public class CameraContent extends BaseContent{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String photo_url;

	public CameraContent(int id, String title, String text) {
		super(id, title, text);
	}

	public CameraContent(int id, String title, String text, String photo_url) {
		this(id, title, text);
		this.photo_url = photo_url;
	}
}
